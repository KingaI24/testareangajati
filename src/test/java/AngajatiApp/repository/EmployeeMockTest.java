package AngajatiApp.repository;

import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.model.Employee;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static AngajatiApp.controller.DidacticFunction.ASISTENT;
import static AngajatiApp.controller.DidacticFunction.LECTURER;
import static org.junit.Assert.*;

public class EmployeeMockTest {

    private EmployeeMock eM;

    @Before
    public void setUp() throws Exception {
        eM = new EmployeeMock();
    }

    @After
    public void tearDown() throws Exception {
        eM = null;
    }

    @Test
    public void TC1() {
        Employee e1 = new Employee();
        e1.setFirstName("Su");
        e1.setLastName("Ni");
        e1.setCnp("2801203124561");
        e1.setFunction(LECTURER);
        e1.setSalary(3500.00);
        int noEmployees = eM.getEmployeeList().size();
        assertTrue(eM.addEmployee(e1));
        assertEquals(String.format("Returned %d, expected %d", eM.getEmployeeList().size(),noEmployees+1),noEmployees+1, eM.getEmployeeList().size());
    }

    @Test
    public void TC2() {
        Employee e1 = new Employee();
        e1.setFirstName("M");
        e1.setLastName("Maria");
        e1.setCnp("2891212123123");
        e1.setFunction(ASISTENT);
        e1.setSalary(2100.00);
        int noEmployees = eM.getEmployeeList().size();
        assertFalse(eM.addEmployee(e1));
        assertNotEquals(String.format("Returned %d, expected %d", eM.getEmployeeList().size(),noEmployees+1),noEmployees+1, eM.getEmployeeList().size());
    }

    @Test
    public void TC3() {
        Employee e1 = new Employee();
        e1.setFirstName("Vlad");
        e1.setLastName("Andrei");
        e1.setCnp("1850312345678");
        e1.setFunction(ASISTENT);
        e1.setSalary(2080.00);
        int noEmployees = eM.getEmployeeList().size();
        assertTrue(eM.addEmployee(e1));
        assertEquals(String.format("Returned %d, expected %d", eM.getEmployeeList().size(),noEmployees+1),noEmployees+1, eM.getEmployeeList().size());
    }

    @Test
    public void TC4() {
        Employee e1 = new Employee();
        e1.setFirstName("Rares");
        e1.setLastName("Miron");
        e1.setCnp("1750101451231");
        e1.setFunction(ASISTENT);
        e1.setSalary(2079.00);
        int noEmployees = eM.getEmployeeList().size();
        assertFalse(eM.addEmployee(e1));
        assertNotEquals(String.format("Returned %d, expected %d", eM.getEmployeeList().size(),noEmployees+1),noEmployees+1, eM.getEmployeeList().size());
    }

    @Test
    public void TC5() {
        Employee e1 = new Employee();
        e1.setFirstName("Rares");
        e1.setLastName("Miron");
        e1.setCnp("175010145123");
        e1.setFunction(ASISTENT);
        e1.setSalary(2080.00);
        int noEmployees = eM.getEmployeeList().size();
        assertFalse(eM.addEmployee(e1));
        assertNotEquals(String.format("Returned %d, expected %d", eM.getEmployeeList().size(),noEmployees+1),noEmployees+1, eM.getEmployeeList().size());
    }

    @Test
    public void TC6() {
        Employee e1 = null;
        String employeeMockList = eM.getEmployeeList().toString();
        eM.modifyEmployeeFunction(e1,DidacticFunction.LECTURER);
        assertTrue(eM.getEmployeeList().toString().equals(employeeMockList));
    }


    @Test
    public void TC7() {
        Employee e1 = new Employee("Ian", "David", "1891122890876", DidacticFunction.ASISTENT, 2500d);
        e1.setId(99);

        String employeeMockList = eM.getEmployeeList().toString();
        for (Employee e : eM.getEmployeeList()){
            if (e.equals(e1)) {
                e1.setId(e.getId());
            }
        };
        eM.modifyEmployeeFunction(e1,DidacticFunction.LECTURER);
        assertTrue(eM.getEmployeeList().toString().equals(employeeMockList));
    }


    @Test
    public void TC8() {
        Employee e1 = new Employee("Marius", "Pacuraru", "1234567890876", DidacticFunction.ASISTENT, 2500d);
        e1.setId(99);

        String employeeMockList = eM.getEmployeeList().toString();
        for (Employee e : eM.getEmployeeList()){
            if (e.equals(e1)) {
                e1.setId(e.getId());
            }
        };
        eM.modifyEmployeeFunction(e1,DidacticFunction.LECTURER);
        assertFalse(eM.getEmployeeList().toString().equals(employeeMockList));
    }

    @Test
    public void TC9() {
        Employee e1 = new Employee("Marius", "Pacuraru", "1234567890876", DidacticFunction.ASISTENT, 2500d);
        e1.setId(99);

        eM.getEmployeeList().clear();
        String employeeMockList = eM.getEmployeeList().toString();

        for (Employee e : eM.getEmployeeList()){
            if (e.equals(e1)) {
                e1.setId(e.getId());
            }
        };

        eM.modifyEmployeeFunction(e1,DidacticFunction.LECTURER);
        assertTrue(eM.getEmployeeList().toString().equals(employeeMockList));
    }

}
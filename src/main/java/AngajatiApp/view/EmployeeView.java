package AngajatiApp.view;

public class EmployeeView {
	
	public void printMenu() {
		System.out.println("--- Menu ---");
		System.out.println("\t1. Adauga angajat nou");
		System.out.println("\t2. Modifica functia didactica a unui angajat");
		System.out.println("\t3. Afiseaza salariati");
		System.out.println("\t4. Afiseaza salariati descrescator dupa salar");
		System.out.println("\t5. Afiseaza salariati crescator dupa varsta");
		System.out.println("\t0. Exit");
	}

}
